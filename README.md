# nanoscopic - Mouse Click Icons

To be able to visualize mouse clicks in various documentation, I've needed some icons. To not get bothered by copyright stuff, I've created my own simple icon set.

<img alt='nanoscopic - Mouse Click Icons' src='https://wiki.nanoscopic.de/lib/exe/fetch.php/tools/graphics/nanoscopic-mouse-click-icons.png?' height="400"/>

## License

[nanoscopic - Mouse Click Icons](https://wiki.nanoscopic.de/doku.php/tools/graphics/nanoscopic_-_mouse_click_icons) by [Heiko Mischer](https://www.nanoscopic.de/) are licensed under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1).

## Links

[Website](https://wiki.nanoscopic.de/doku.php/tools/graphics/nanoscopic_-_mouse_click_icons)

[Repository](https://codeberg.org/rokkitlawnchair/nanoscopic-mouse-click-icons)